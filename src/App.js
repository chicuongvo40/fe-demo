import logo from './logo.svg';
import './App.css';
import { useEffect } from 'react';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@microsoft/signalr';


function App() {
  useEffect(()=>{
    const init=async()=>{
      const conn=new HubConnectionBuilder()
      .withUrl("https://cuong.twentytwo.asia/chat")
      .withAutomaticReconnect()
      .configureLogging(LogLevel.Information)
      
      .build()

      await conn.start().then(()=>{console.log("connected");})
    }
    init()
  },[])
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        
      </header>
    </div>
    
  );
}

export default App;
